﻿using ChinookTunes.Repositories;
using System.Collections.Generic;
using ChinookTunes.Models;
using System;

namespace ChinookTunes
{
    class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository customerRepo = new CustomerRepository();
            // Query 1
            IEnumerable<Customer> allCustomers = GetAllCustomers(customerRepo);

            // Query 2
            Customer customerById = GetCustomerById(customerRepo);

            // Query 3
            Customer customerByName = GetCustomerByName(customerRepo);

            // Query 4
            IEnumerable<Customer> customerPage = GetCustomerPage(customerRepo);

            // Query 5
            bool customerAdded = CreateNewCustomer(customerRepo);

            // Query 6
            bool upateCustomer = UpdateCustomer(customerRepo);

            // Query 7
            IEnumerable<CustomerCountry> customerCountryList = CountryCounter(customerRepo);

            // Query 8
            IEnumerable<CustomerSpender> bigSpenders = GetBigSpenders(customerRepo);

            // Query 9
            IEnumerable<CustomerGenre> customerGenreList = GetFavouriteGenre(customerRepo);
        }

        private static IEnumerable<CustomerGenre> GetFavouriteGenre(ICustomerRepository repo)
        {
            return repo.GetFavouriteGenre(1);
        }

        private static IEnumerable<Customer> GetAllCustomers(ICustomerRepository repo)
        {
            return repo.GetAll();
        }

        private static Customer GetCustomerById(ICustomerRepository repo)
        {
            return repo.GetById(1);
        }

        private static Customer GetCustomerByName(ICustomerRepository repo)
        {
            return repo.GetCustomerByName("Daan");
        }

        private static IEnumerable<Customer> GetCustomerPage(ICustomerRepository repo)
        {
            return repo.GetPage(5, 2);
        }

        private static bool CreateNewCustomer(ICustomerRepository repo)
        {
            return repo.CreateNewCustomer("Jasper", "Klingen", "Netherlands", "1111AA", "555", "Damn@son.nl");
        }

        private static bool UpdateCustomer(ICustomerRepository repo)
        {
            return repo.UpdateName(6, "Helena Updated");
        }

        private static IEnumerable<CustomerCountry> CountryCounter(ICustomerRepository repo)
        {
            return repo.CountryCounter();
        }

        private static IEnumerable<CustomerSpender> GetBigSpenders(ICustomerRepository repo)
        {
            return repo.GetBigSpenders();
        }
    }
}
