﻿namespace ChinookTunes.Models
{
    public class CustomerSpender
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal Total { get; set; }

        public override string ToString()
        {
            return $"{FirstName} {LastName}: {Total}";
        }
    }
    
}
