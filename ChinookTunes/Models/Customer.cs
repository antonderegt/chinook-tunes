﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChinookTunes.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public int SupportRepId { get; set; }

        public string PrintCustomer()
        {
            return CustomerId + " - " + FirstName + " - " + LastName + " - " + Company + " - " + Address + " - " + City + " - " + State + " - " + Country + " - " + PostalCode + " - " + Phone + " - " + Fax + " - " + Email + " - " + SupportRepId;
        }
        public string PrintHeader()
        {
            return "CustomerId \t FirstName \t LastName \t Company \t Address  City  State  Country + PostalCode + Phone + Fax + Email + SupportRepId";
        }
    }

}
