﻿namespace ChinookTunes.Repositories
{
    public class CustomerGenre
    {
        public string CustomerName { get; set; }
        public string Genre { get; set; }
    }
}