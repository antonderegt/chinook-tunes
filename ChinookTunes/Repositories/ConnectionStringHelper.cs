﻿using Microsoft.Data.SqlClient;
using System;

namespace ChinookTunes.Repositories
{
    public class ConnectionStringHelper
    {
        /// <summary>
        /// Build the connection string based on the environment.
        /// </summary>
        /// <returns>Connection string</returns>
        public static string GetConnectionString()
        {
            AskForInput();
            string dataSourceInput = Console.ReadLine();
            int dataSource;

            while (!int.TryParse(dataSourceInput, out dataSource) || dataSource < 1 || dataSource > 2)
            {
                AskForInput();
                dataSourceInput = Console.ReadLine();
            }

            Console.Clear();

            SqlConnectionStringBuilder builder = new();
            builder.DataSource = dataSource == 1 ? "DESKTOP-F12R4UK\\" : "DESKTOP-J1623AP\\SQLEXPRESS";
            builder.InitialCatalog = "Chinook";
            builder.IntegratedSecurity = true;

            return builder.ConnectionString;
        }

        /// <summary>
        /// Asks the user for which connection string to use.
        /// </summary>
        private static void AskForInput()
        {
            Console.Clear();
            Console.WriteLine("Who are you?");
            Console.WriteLine("1. Jasper");
            Console.WriteLine("2. Anton");
        }
    }
}
