﻿using ChinookTunes.Models;
using System.Collections.Generic;

namespace ChinookTunes.Repositories
{
    public interface ICustomerRepository
    {
        public Customer GetById(int id);
        public Customer GetCustomerByName(string partialname);
        public bool UpdateName(int id, string name);
        public bool CreateNewCustomer(string firstName, string lastName, string country, string postalCode, string phoneNumber, string email);
        public IEnumerable<Customer> GetAll();
        public IEnumerable<Customer> GetPage(int offset, int limit);
        public IEnumerable<CustomerGenre> GetFavouriteGenre(int id);
        public IEnumerable<CustomerCountry> CountryCounter();
        public IEnumerable<CustomerSpender> GetBigSpenders();
    }
}
