﻿using ChinookTunes.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;

namespace ChinookTunes.Repositories
{
    class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Query 1, Read all the customers in the database, this should display their: Id, first name, last name, country, postal code, phone number and email
        /// </summary>
        /// <returns>List of all customers</returns>
        public IEnumerable<Customer> GetAll()
        {
            List<Customer> customerList = new();
            string sqlSelect = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                using (SqlConnection connection = new(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand commandSelect = new(sqlSelect, connection))
                    {
                        using (SqlDataReader reader = commandSelect.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new()
                                {
                                    CustomerId = reader.GetInt32(0),
                                    FirstName = !reader.IsDBNull(1) ? reader.GetString(1) : "",
                                    LastName = !reader.IsDBNull(2) ? reader.GetString(2) : "",
                                    Country = !reader.IsDBNull(3) ? reader.GetString(3) : "",
                                    PostalCode = !reader.IsDBNull(4) ? reader.GetString(4) : "",
                                    Phone = !reader.IsDBNull(5) ? reader.GetString(5) : "",
                                    Email = !reader.IsDBNull(6) ? reader.GetString(6) : "",
                                };

                                customerList.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }

            return customerList;
        }

        /// <summary>
        /// Query 2, Read a specific customer from the database(by Id)
        /// </summary>
        /// <param name="id">customer ID</param>
        /// <returns>Single customer with specific id number</returns>
        public Customer GetById(int id)
        {
            Customer customer = new();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = @Id";
            try
            {
                using (SqlConnection connection = new(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new(sql, connection))
                    {
                        command.Parameters.AddWithValue("@Id", id);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer = new()
                                {
                                    CustomerId = reader.GetInt32(0),
                                    FirstName = !reader.IsDBNull(1) ? reader.GetString(1) : "",
                                    LastName = !reader.IsDBNull(2) ? reader.GetString(2) : "",
                                    Country = !reader.IsDBNull(3) ? reader.GetString(3) : "",
                                    PostalCode = !reader.IsDBNull(4) ? reader.GetString(4) : "",
                                    Phone = !reader.IsDBNull(5) ? reader.GetString(5) : "",
                                    Email = !reader.IsDBNull(6) ? reader.GetString(6) : "",
                                };
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }

            return customer;
        }

        /// <summary>
        /// Query 3,  Read a specific customer from the database(by Name)
        /// </summary>
        /// <param name="partialname">Part of first or lastname</param>
        /// <returns>First customer from a list of customers who matches the partial search query</returns>
        public Customer GetCustomerByName(string partialname)
        {
            Customer customer = new Customer();
            string sql = "SELECT TOP 1 CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName LIKE @PartialName OR LastName LIKE @PartialName";
            try
            {
                using (SqlConnection connection = new(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand commandSelect = new(sql, connection))
                    {
                        commandSelect.Parameters.AddWithValue("@PartialName", "%" + partialname + "%");
                        using (SqlDataReader reader = commandSelect.ExecuteReader())
                        {
                            reader.Read();
                            customer = new()
                            {
                                CustomerId = reader.GetInt32(0),
                                FirstName = !reader.IsDBNull(1) ? reader.GetString(1) : "",
                                LastName = !reader.IsDBNull(2) ? reader.GetString(2) : "",
                                Country = !reader.IsDBNull(3) ? reader.GetString(3) : "",
                                PostalCode = !reader.IsDBNull(4) ? reader.GetString(4) : "",
                                Phone = !reader.IsDBNull(5) ? reader.GetString(5) : "",
                                Email = !reader.IsDBNull(6) ? reader.GetString(6) : ""
                            };

                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }

            return customer;
        }

        /// <summary>
        /// Query 4, Return a page of customers from the database
        /// </summary>
        /// <param name="offset">Starting point</param>
        /// <param name="limit">Number of records</param>
        /// <returns>List of custormers within a specific range</returns>
        public IEnumerable<Customer> GetPage(int offset, int limit)
        {
            List<Customer> customerList = new();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId BETWEEN @Offset and @Limit";
            try
            {
                using (SqlConnection connection = new(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand commandSelect = new(sql, connection))
                    {
                        commandSelect.Parameters.AddWithValue("@Offset", offset);
                        commandSelect.Parameters.AddWithValue("@Limit", offset + limit - 1);

                        using (SqlDataReader reader = commandSelect.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new()
                                {
                                    CustomerId = reader.GetInt32(0),
                                    FirstName = !reader.IsDBNull(1) ? reader.GetString(1) : "",
                                    LastName = !reader.IsDBNull(2) ? reader.GetString(2) : "",
                                    Country = !reader.IsDBNull(3) ? reader.GetString(3) : "",
                                    PostalCode = !reader.IsDBNull(4) ? reader.GetString(4) : "",
                                    Phone = !reader.IsDBNull(5) ? reader.GetString(5) : "",
                                    Email = !reader.IsDBNull(6) ? reader.GetString(6) : "",
                                };

                                customerList.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }

            return customerList;
        }

        /// <summary>
        /// Query 5, Add a new customer to the database
        /// </summary>
        /// <param name="firstName">First Name</param>
        /// <param name="lastName">Last Name</param>
        /// <param name="country">Country</param>
        /// <param name="postalCode">Postal Code</param>
        /// <param name="phoneNumber">Phone Number</param>
        /// <param name="email">E-mail Adress</param>
        /// <returns>A boolean wether the insert was successfull</returns>
        public bool CreateNewCustomer(string firstName, string lastName, string country, string postalCode, string phoneNumber, string email)
        {
            bool success = false;
            string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                using (SqlConnection connection = new(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new(sql, connection))
                    {
                        command.Parameters.AddWithValue("@FirstName", firstName);
                        command.Parameters.AddWithValue("@LastName", lastName);
                        command.Parameters.AddWithValue("@Country", country);
                        command.Parameters.AddWithValue("@PostalCode", postalCode);
                        command.Parameters.AddWithValue("@Phone", phoneNumber);
                        command.Parameters.AddWithValue("@Email", email);

                        success = command.ExecuteNonQuery() > 0;
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return success;
        }

        /// <summary>
        /// Query 6, Update an existing customer
        /// </summary>
        /// <param name="id">Customer ID</param>
        /// <param name="name">Customer Name</param>
        /// <returns>A boolean wether or not updated was successfull</returns>
        public bool UpdateName(int id, string name)
        {
            bool success = false;
            string sql = "UPDATE Customer SET FirstName = @NewFirstName WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection connection = new(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new(sql, connection))
                    {
                        command.Parameters.AddWithValue("@NewFirstName", name);
                        command.Parameters.AddWithValue("@CustomerId", id);

                        success = command.ExecuteNonQuery() > 0;
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }

            return success;
        }

        /// <summary>
        /// Query 7, Return the number of customers in each country, ordered descending
        /// </summary>
        /// <returns>A list of countries and the amounth of customers in that country</returns>
        public IEnumerable<CustomerCountry> CountryCounter()
        {
            List<CustomerCountry> countryCounts = new List<CustomerCountry>();
            string sql = "SELECT c.Country, COUNT(*) AS count FROM Customer AS c GROUP BY Country ORDER BY count DESC";
            try
            {
                using (SqlConnection connection = new(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand commandSelect = new(sql, connection))
                    {
                        using (SqlDataReader reader = commandSelect.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                countryCounts.Add(new CustomerCountry()
                                {
                                    Count = reader.GetInt32(1),
                                    Country = reader.GetString(0)
                                });
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }

            return countryCounts;
        }
        /// <summary>
        /// Query 8, Customers who are the highest spenders, orderd descending
        /// </summary>
        /// <returns>List of biggeste spenders, biggest spender at the top</returns>
        public IEnumerable<CustomerSpender> GetBigSpenders()
        {
            List<CustomerSpender> bigSpenders = new();
            string sql = "SELECT c.FirstName, c.LastName, SUM(i.Total) AS sum " +
                         "FROM Invoice AS i JOIN Customer AS c ON c.CustomerId = i.CustomerId " +
                         "GROUP BY i.CustomerId, c.FirstName, c.LastName " +
                         "ORDER BY sum DESC; ";
            try
            {
                using (SqlConnection connection = new(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand commandSelect = new(sql, connection))
                    {
                        using (SqlDataReader reader = commandSelect.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender spender = new()
                                {
                                    FirstName = reader.GetString(0),
                                    LastName = reader.GetString(1),
                                    Total = reader.GetDecimal(2)
                                };
                                bigSpenders.Add(spender);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }

            return bigSpenders;
        }
        
        /// <summary>
        /// Query 9, Get favourite genre for specific customer by Id
        /// </summary>
        /// <param name="id">Customer ID</param>
        /// <returns>A List of the customers favourite genre</returns>
        public IEnumerable<CustomerGenre> GetFavouriteGenre(int id)
        {
            List<CustomerGenre> customerGenreList = new();
            string sql =    "SELECT g.Name, COUNT(g.Name) AS genrecount, c.FirstName " +
                            "INTO #Populargenre FROM Customer AS c " +
                            "JOIN Invoice AS i ON c.CustomerId = i.CustomerId " +
                            "JOIN InvoiceLine AS il ON i.InvoiceId = il.InvoiceId " +
                            "JOIN Track AS t ON il.TrackId = t.TrackId " +
                            "JOIN Genre AS g ON g.GenreId = t.GenreId " +
                            "WHERE c.CustomerId = @CustomerId " +
                            "GROUP BY c.FirstName, g.Name " +
                            "ORDER BY genrecount DESC; " +

                            "SELECT FirstName, Name " +
                            "FROM #Populargenre " +
                            "GROUP BY FirstName, Name, genrecount " +
                            "HAVING genrecount = (SELECT MAX(genrecount) " +
                            "FROM #Populargenre) " +
                            "ORDER BY genrecount DESC; " +

                            "DROP TABLE #Populargenre; ";
            try
            {
                using (SqlConnection connection = new(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand commandSelect = new(sql, connection))
                    {
                        commandSelect.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = commandSelect.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre customerGenre = new()
                                {
                                    CustomerName = reader.GetString(0),
                                    Genre = reader.GetString(1)
                                };
                                customerGenreList.Add(customerGenre);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return customerGenreList;
        }
    }

}
